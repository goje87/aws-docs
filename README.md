_Creating an Instance for AWS CodeDeploy_

Follow steps provided at http://docs.aws.amazon.com/codedeploy/latest/userguide/how-to-set-up-new-instance.html

1. Click on 'Launch Instance' in AWS console.
2. Select Ubuntu image.
3. Choose your preferred instance type and click on 'Configure Instance Details'.
4. For 'IAM role' select option that begins with 'CodeDeploySampleStack...'.
5. Under 'Advanced Details` section, use contents of https://gitlab.com/goje87/aws-docs/blob/master/setup-instance-ubuntu.sh
6. Click 'Review and Launch'