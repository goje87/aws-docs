#!/bin/bash

# Upload this file when creating new instance, under 'Advanced Details' section
# in 'Step 3: Configure Instance Details'.


# Installing codeDeploy agent
# Ref: http://docs.aws.amazon.com/codedeploy/latest/userguide/how-to-set-up-new-instance.html

REGION_NAME="us-east-1"

apt-get -y update
apt-get -y install awscli
apt-get -y install ruby
cd /home/ubuntu
aws s3 cp s3://aws-codedeploy-`$REGION_NAME`/latest/install . --region `$REGION_NAME`
chmod +x ./install
./install auto


# Installing nodeJs
# Ref: http://tecadmin.net/install-latest-nodejs-npm-on-ubuntu/#

apt-get -y install python-software-properties
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
apt-get -y install nodejs


# Installing mongo db
# Ref: https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list
apt-get update
apt-get -y install mongodb-org
service mongod start


# Installing lets encrypt
# Ref: https://certbot.eff.org/#ubuntuxenial-other

apt-get -y install letsencrypt